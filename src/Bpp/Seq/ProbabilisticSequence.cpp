//
// File: ProbabilisticSequence.cpp
// Created by: Murray Patterson
// Created on: Thurs Sep 11
//

#include "ProbabilisticSequence.h" // include its header file

#include <Bpp/Text/TextTools.h>

using namespace bpp;

// Constructors

ProbabilisticSequence::ProbabilisticSequence(const Alphabet * alpha) :
  name_(),
  alphabet_(alpha),
  data_(0),
  comments_()
{}

ProbabilisticSequence::ProbabilisticSequence(const std::string & name, const DataTable & data, const Alphabet * alpha) throw (Exception) :
  name_(name),
  alphabet_(alpha),
  data_(0),
  comments_(0)
{
  setData(data);
}

ProbabilisticSequence::ProbabilisticSequence(const std::string & name, const DataTable & data, const Comments & comments, const Alphabet * alpha) throw (Exception) :
  name_(name),
  alphabet_(alpha),
  data_(0),
  comments_(comments)
{
  setData(data);
}

// Copy Constructor

ProbabilisticSequence::ProbabilisticSequence(const ProbabilisticSequence & s) :
  name_(s.getName()),
  alphabet_(s.getAlphabet()),
  data_(s.getData()),
  comments_(s.getComments())
{}

// Assignment Operator

ProbabilisticSequence & ProbabilisticSequence::operator=(const ProbabilisticSequence & s)
{
  name_ = s.getName();
  alphabet_ = s.getAlphabet();
  data_ = s.getData();
  comments_ = s.getComments();
  return *this;
}

/****************************************************************************************/

// Methods

const std::vector<std::string> ProbabilisticSequence::getColumns() const
{

  std::vector<std::string> v;
  for(std::vector<std::string>::const_iterator s = alphabet_->getSupportedChars().begin(); s != alphabet_->getSupportedChars().end(); ++s) {
    if(!alphabet_->isGap(*s) && !alphabet_->isUnresolved(*s))
      v.push_back(*s);
  }

  return v;
}

/****************************************************************************************/

const std::vector<std::string> ProbabilisticSequence::getPdf(std::size_t pos) const throw (IndexOutOfBoundsException)
{
  return data_.getRow(pos);
}

/****************************************************************************************/

void ProbabilisticSequence::setData(const DataTable & data) throw (Exception)
{

  // first, if table has column names, we run these names against the
  // alphabet to see if they match.  Note: we ignore row names -- they
  // serve us no purpose here
  if(data.hasColumnNames()) {
    
    // compare directly the Data Table columns with that of the
    // alphabet.  Note: getColumnNames could throw a
    // NoTableColumnNamesException, but we don't try to catch this
    // because we did a check above for hasColumnNames
    if(data.getColumnNames() != getColumns())
      throw Exception("ProbabilisticSequence::setData. Column names do not match alphabet");
  }
  else { // DataTable has no column names

    // hence, we first check if width of DataTable is exactly that of
    // the alphabet size.  Note: getSize returns the number of
    // *resolved* characters (what we want, I think)
    if(data.getNumberOfColumns() != alphabet_->getSize())
      throw DimensionException("ProabilisticSequence::setData", data.getNumberOfColumns(), alphabet_->getSize());
  }

  // in either case, the check passes, so now we do a pass over the
  // table to ensure that each entry is a decimal number, and that
  // each row sums up to 1
  for(std::size_t i = 0; i < data.getNumberOfRows(); ++i) {

    // here we take a row at a time
    std::vector<std::string> v(data.getRow(i));

    // take the sum (where toDouble ensures that v[s] is a decimal number)
    double sum = 0;
    for(std::vector<std::string>::const_iterator s = v.begin(); s != v.end(); ++s)
      sum += TextTools::toDouble(*s,'.','E');

    // then we ensure that the sum is 1
    double tol = 0.000001; // but within a tolerance
    double lowerbound = double(1) - tol;
    double upperbound = double(1) + tol;
    if(sum < lowerbound or sum > upperbound)
      throw Exception("ProbabilisticSequence::setData. Probabilities do not sum up to 1");
  }

  data_ = data; // final check passes, data_ becomes DataTable

  // now, we work with the columns of our DataTable, in the case that
  // it has no column names
  if(!data.hasColumnNames()) {

    /*
      we associate the columns of DataTable with the (non-gap
      resolved) characters of the alphabet ... this will work with,
      i.e., binary alphabets and DNA alphabets (probably only going to
      use ProbabilisticSequence for binary alphabets anyway)
    */

    std::vector<std::string> columnNames;
    for(std::vector<std::string>::const_iterator i = alphabet_->getSupportedChars().begin(); i != alphabet_->getSupportedChars().end(); ++i)
      if(!alphabet_->isGap(*i) and !alphabet_->isUnresolved(*i))
	columnNames.push_back(*i);

    // set the names.  Note: there should not be a DimensionException
    // because we check above for size, but in any case this method
    // throws DimensionExceptions.  This method does not, however,
    // throw DuplicatedTableColumnNameExceptions, however, we assume
    // that Alphabet already disallows duplicated characters
    data_.setColumnNames(columnNames);
  }
}
