//
// File: ProbabilisticSequence.h
// Created by: Murray Patterson
// Created on: Wed Sep 10 2014
//

#ifndef _PROBABILISTIC_SEQUENCE_H_
#define _PROBABILISTIC_SEQUENCE_H_

#include "Sequence.h"
#include <Bpp/Numeric/DataTable.h>

namespace bpp
{

  class ProbabilisticSequence :
    public Sequence
  {

  private :

    std::string name_;
    const Alphabet * alphabet_;
    DataTable data_;
    Comments comments_;
    
  public :

    // Constructors

    ProbabilisticSequence(const Alphabet * alpha);

    ProbabilisticSequence(const std::string & name, const DataTable & data, const Alphabet * alpha) throw (Exception);

    ProbabilisticSequence(const std::string & name, const DataTable & data, const Comments & comments, const Alphabet * alpha) throw (Exception);

    // Copy Constructor

    ProbabilisticSequence(const ProbabilisticSequence & s);

    // Assignment Operator

    ProbabilisticSequence & operator=(const ProbabilisticSequence & s);

    // Clone

    ProbabilisticSequence * clone() const { return new ProbabilisticSequence(*this); }

    // Destructor

    virtual ~ProbabilisticSequence() {}

  public :

    // get name, alphbet, comments and size

    const std::string & getName() const { return name_; }
    const Alphabet * getAlphabet() const { return alphabet_; }
    const std::vector<std::string> getColumns() const;
    const Comments & getComments() const { return comments_; }
    virtual size_t size() const { return data_.getNumberOfRows(); }

    // set name and comments

    void setName(const std::string & name) { name_ = name; }
    void setComments(const Comments & comments) { comments_ = comments; }

    // get content

    // cannot call it 'getContent' (well I can, now), so (but) I use this instead
    const DataTable & getData() const { return data_; }

    // we need this one as well : get a value at a position, but I
    // cannot use 'getValue' so I called it getPdf (the discrete PDF
    // over the alphabet at a position)
    const std::vector<std::string> getPdf(size_t pos) const throw (IndexOutOfBoundsException);

    // set content

    // this one could actually have a meaning, i.e., assigning
    // probability 1.0 to the character appearing at a site in the
    // sequence, and 0.0 to all else, but we leave it empty for now
    virtual void setContent(const std::string & sequence) throw (BadCharException) {}

    // this one could also have a meaning analagous to the above
    virtual void setContent(const std::vector<int> & list) throw (BadIntException) {}
    
    virtual void setContent(const std::vector<std::string> & list) throw (BadCharException) {}

    // the one we really need here
    void setData(const DataTable & data) throw (Exception);

    // resize

    // these could have meaning: simply override for now
    virtual void setToSizeR(size_t newSize) {}
    virtual void setToSizeL(size_t newSize) {}

    // append

    // these could also have meaning: simply override for now
    virtual void append(const Sequence & seq) throw (AlphabetMismatchException) {}
    virtual void append(const std::vector<int>& content) throw (BadIntException) {}
    virtual void append(const std::vector<std::string>& content) throw (BadCharException) {}
    virtual void append(const std::string& content) throw (BadCharException) {}
    
    /*
      we could have this one as well (not implemented yet) : it would
      throw a DimensionException if datatables don't match in
      dimension, but also an exception if the names of the columns do
      not match.  also, should it be virtual (like in BasicSequence)?
    */
    // void append(const DataTable & data) throw (DimensionException);

    /*
      maybe we should restrict the user to only this type of append,
      so that all we have to worry about is whether or not the
      alphabets match ... let's see
    */
    // void append(const ProbabilisticSequence & s) throw (AlphabetMismatchException);

    /*
      another possible improvement could be to define a
      ProbabilisticSymbolList, since BasicSequence has such a class
      that it inherits from, and this is sort of an analog or parallel
      of BasicSequence.  However I'm not sure what purpose this serves
      BasicSequence: perhaps it just decouples it so that one could
      define another type of Sequence based on a BasicSymbolList.
      Even if so, not sure how likely someone will define another type
      of Sequence based on a ProbabilisticSymbolList, so I keep it
      simple (coupled) for now
    */

    /*
      okay, perhaps this BasicSymbolList allows to hide all these
      overridings of the (less meaningful?) pure virtual functions in
      SymbolList ... but I'm just going to do it here (note that in my
      case, they have either little or in most cases no possible
      meaning).  Neither way seems cleaner to me, because with
      inheriting from Sequence and a more specific type of SymbolList,
      I will have getters and setters, etc. in separate files
      ... seems a bit messy ... but then again so is the below
    */

    virtual std::string toString() const { return ""; }

    virtual void addElement(const std::string & c) throw (BadCharException) {}

    virtual void addElement(size_t pos, const std::string & c) throw (BadCharException, IndexOutOfBoundsException) {}

    virtual void setElement(size_t pos, const std::string & c) throw (BadCharException, IndexOutOfBoundsException) {}

    // this one could possibly have meaning (i.e., delete
    // corresponding row in the DataTable)
    virtual void deleteElement(size_t pos) throw (IndexOutOfBoundsException) {}

    // this one as well (i.e., delete corresponding rows in the
    // DataTable)
    virtual void deleteElements(size_t pos, size_t len) throw (IndexOutOfBoundsException) {}

    virtual std::string getChar(size_t pos) const throw (IndexOutOfBoundsException) { return ""; }

    virtual void addElement(int v) throw (BadIntException) {}

    virtual void addElement(size_t pos, int v) throw (BadIntException, IndexOutOfBoundsException) {}

    virtual void setElement(size_t pos, int v) throw (BadIntException, IndexOutOfBoundsException) {}

    virtual int getValue(size_t pos) const throw (IndexOutOfBoundsException) { return 0; }

    virtual const int & operator[](size_t i) const { return *(new int(0)); }
    virtual int & operator[](size_t i) { return *(new int(0)); }

    // this one could possibly have meaning (shuffle the rows in the
    // DataTable, but .. ehh)
    virtual void shuffle() {}

  };

} // end of namespace bpp

#endif // _PROBABILISTIC_SEQUENCE_H_
