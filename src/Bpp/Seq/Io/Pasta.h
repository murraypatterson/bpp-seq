//
// File: Pasta.h
// Created by: Murray Patterson
// Created on: Tuesday Oct 28
//

#ifndef _BPP_SEQ_IO_PASTA_H_
#define _BPP_SEQ_IO_PASTA_H_

#include "Fasta.h"
#include "../ProbabilisticSequence.h"

namespace bpp
{

  class Pasta :
    public Fasta
  {

  private :

    bool charsByLine_;
    bool checkNames_;
    bool extended_;
    bool strictNames_;

  public :

    // Constructors

    Pasta(unsigned int charsByLine = 100, bool checkSequenceNames = true, bool extended = false, bool strictSequenceNames = false) : charsByLine_(charsByLine), checkNames_(checkSequenceNames), extended_(extended), strictNames_(strictSequenceNames) {}

    // Destructor

    virtual ~Pasta() {}

  public :

    const std::string getFormatName() const { return "PASTA file"; }

    bool nextSequence(std::istream & input, ProbabilisticSequence & seq) const throw (Exception);

    void writeSequence(std::ostream & output, const ProbabilisticSequence & seq) const throw (Exception);

    void appendSequencesFromStream(std::istream & input, SequenceContainer & sc) const throw (Exception);

  };

} // end of namespace bpp

#endif // _BPP_SEQ_IO_PASTA_H_
