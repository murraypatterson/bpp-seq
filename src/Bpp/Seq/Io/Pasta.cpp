//
// File: Pasta.cpp
// Created by: Murray Patterson
// Created on: Tuesday Oct 28
//

#include "Pasta.h" // include its header file

#include <fstream>

#include <Bpp/Numeric/DataTable.h>

using namespace bpp;
using namespace std;

// Methods

bool Pasta::nextSequence(istream & input, ProbabilisticSequence & seq) const throw (Exception) {

  if(!input)
    throw IOException("Pasta::nextSequence : can't read from istream input");

  string seqname = "";
  vector<string> v;
  Comments seqcmts;
  short seqcpt = 0;
  string linebuffer = "";
  char c;

  while(!input.eof()) {

    c = static_cast<char>(input.peek());
    if(input.eof())
      c = '\n';

    // Detect the beginning of a sequence
    if(c == '>') {

      // Stop if we find a new sequence
      if(seqcpt++)
	break;
    }

    getline(input, linebuffer);

    if(c == '>') {

      // Get the sequence name line
      seqname = string(linebuffer.begin() + 1, linebuffer.end());
    }

    /*
      While dealing with all the formatting, names, etc., including
      strictNames and extended formats will all be the same as with
      the Fasta format, this is where it will be radically different :
      the handling of the sequence content

      For the moment, I'm keeping it simple : assuming that all of the
      'sequence' has a 'width' of 1, i.e., one can read it line by
      line, like we do below, that is, that it pertains to a binary
      alphabet (and moreover, the probability that the character is
      1).  However, one can make this much more general -- for general
      alphabets, where the 'width' would be the size S of the
      alphabet, i.e., we would read in S lines at a time -- something
      I may come back and do someday

      Note also, that the goal here is to feed this 'sequence' into a
      ProbabilisticSequence object, which contains a DataTable, where
      the intension was to read column-wise 'sequences' with
      DataTable's read function, yet I'm now feeding this row-wise
      sequence into (a column of the DataTable of)
      ProbabilisticSequence.  This is because I like the Pasta format
      as is, but in the future, another generalization could be to
      really have this column-wise type of Pasta format ... we'll
      cross that river when we get to it I suppose
    */

    if(c != '>' && !TextTools::isWhiteSpaceCharacter(c)) {

      // Sequence content

      // we take in a row-wise sequence, not a column-wise one
      StringTokenizer st(linebuffer, " \t\n", false, false);
      while(st.hasMoreToken()) {

	string s = st.nextToken();
	v.push_back(s);
      }
    }
  }

  bool res = (!input.eof());

  // Sequence name and comments isolation (identical to that of Fasta)
  if(strictNames_ || extended_) {

    size_t pos = seqname.find_first_of(" \t\n");
    string seqcmt;

    if(pos != string::npos) {
      seqcmt = seqname.substr(pos + 1);
      seqname = seqname.substr(0, pos);
    }

    if(extended_) {
      StringTokenizer st(seqcmt, " \\", true, false);
      while(st.hasMoreToken()) {
	seqcmts.push_back(st.nextToken());
      }
    }
    else {
      seqcmts.push_back(seqcmt);
    }
    seq.setComments(seqcmts);
  }

  seq.setName(seqname);

  // finally, add pairs of probabilities that (binary) character is 0, resp. 1

  // setup the names first
  string names[] = {"0","1"};
  vector<string> n(names,names+2);
  DataTable data(n); // a data table with 2 columns for p(0), p(1)

  // now add the rows
  for(vector<string>::const_iterator i = v.begin(); i != v.end(); ++i) {

    // this would throw an exception if v[i] is not a properly
    // formatted double : a check that we want to have
    /*
      note : this way of obtaining p(0) from p(1) could render two p's
      of different precisions, I suppose an improvement would be to go
      into the java code of Priam itself and have it output a higher
      precision p(0), in addition to what it does now, i.e., output
      but a higher precision p(1) ... but this seems like overkill.
      What I will do instead is add a sanity check that p(0) + p(1) ==
      1 into ProbabilisticSequence : again this does not solve the
      issue above, but it may reveal cases where it rears its ugly
      head.  Should we reveal such cases, maybe we make said mod
    */
    int precision = 10; // I think this will be more than enough
    string pair[] = {TextTools::toString(double(1) - TextTools::toDouble(*i,'.','E'), precision), *i};
    vector<string> p(pair,pair+2);
    data.addRow(p); // p(0), p(1)
  }

  // and finally we set the data table of the sequence : it handles a
  // NumberFormatException (a good design in general for
  // ProbabilisticSequences, since they may not come only from Pasta
  // readers), but NumberFormatExceptions here have already been
  // handled implictly by toDouble above
  seq.setData(data);

  return res;
}

/****************************************************************************************/

void Pasta::writeSequence(ostream & output, const ProbabilisticSequence & seq) const throw (Exception) {

  if(!output)
    throw IOException("Pasta::writeSequence : can't write to ostream output");

  // Sequence name
  output << ">" << seq.getName();

  // Sequence comments
  if(extended_) {
    for(unsigned int i = 0; i < seq.getComments().size(); ++i) {
      output << " \\" << seq.getComments()[i];
    }
  }
  output << endl;

  /*
    Again, here is where we radically diverge from the Fasta way : in
    the sequence content handling.  For now we assume that the
    DataTable (within the ProbabilisticSequence) contains exactly one
    column, pertaining to the probability that the binary character is
    1.  We output this column as a single (pasta-style) row
  */

  // Sequence content
  vector<string> v = seq.getData().getColumn("1");

  vector<string>::iterator i = v.begin();
  if(i != v.end()) // output the first element
    output << *i;
  for( ; i != v.end(); ++i) // output the rest preceeded by a space
    output << " " + *i;
  output << endl;
}

/****************************************************************************************/

void Pasta::appendSequencesFromStream(istream & input, SequenceContainer & vsc) const throw (Exception) {

  if(!input)
    throw IOException("Pasta::appendFromStream: can't read from istream input");

  char c = '\n';
  char last_c;
  bool header = false;
  bool hasSeq = true;
  string line = "";
  Comments cmts;

  while(!input.eof() && hasSeq) {

    last_c = c;
    input.get(c);

    // Detect the header
    if(extended_ && c == '#') {
      header = true;
      continue;
    }

    // Detect the end of the header
    if(c == '\n') {
      if(extended_ && header) {
	if(line[0] == '\\') {

	  line.erase(line.begin());
	  cmts.push_back(line);
	}
	line = "";
	header = false;
      }
      continue;
    }

    // Capture the header
    if(header) {
      line.append(1,c);
    }

    // Detect the sequence
    if(c == '>' && last_c == '\n') {

      input.putback(c);
      c = last_c;
      ProbabilisticSequence tmpseq(vsc.getAlphabet()); // the only change from the Fasta method
      hasSeq = nextSequence(input, tmpseq);
      vsc.addSequence(tmpseq, checkNames_);
    }
  }
  if(extended_ && cmts.size()) {
    vsc.setGeneralComments(cmts);
  }
}
