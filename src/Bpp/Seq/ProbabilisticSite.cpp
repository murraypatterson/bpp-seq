//
// File: ProbabilisticSite.cpp
// Created by: Murray Patterson
// Created on: Mon Nov 03
//

#include "ProbabilisticSite.h" // include its header file

using namespace bpp;

// aux method used in constructors
std::vector<std::string> getColumns(const Alphabet * alpha);

// Constructors

ProbabilisticSite::ProbabilisticSite(const Alphabet * alpha) :
  Site(alpha),
  data_(alpha->getSize())
{
  std::vector<std::string> cols = getColumns(getAlphabet());
  data_.setColumnNames(cols);
}

ProbabilisticSite::ProbabilisticSite(const Alphabet * alpha, int position) :
  Site(alpha, position),
  data_(alpha->getSize())
{
  std::vector<std::string> cols = getColumns(getAlphabet());
  data_.setColumnNames(cols);
}

ProbabilisticSite::ProbabilisticSite(const DataTable & data, const Alphabet * alpha) throw (Exception) :
  Site(alpha),
  data_(0)
{
  setData(data);
}

ProbabilisticSite::ProbabilisticSite(const DataTable & data, const Alphabet * alpha, int position) throw (Exception) :
  Site(alpha, position),
  data_(0)
{
  setData(data);
}

// Copy Constructors

ProbabilisticSite::ProbabilisticSite(const Site & s) :
  Site(s.getAlphabet(), s.getPosition()),
  data_(0)
{}

ProbabilisticSite::ProbabilisticSite(const ProbabilisticSite & s) :
  Site(s.getAlphabet(), s.getPosition()),
  data_(s.getData())
{}

// Assignment Operator

ProbabilisticSite & ProbabilisticSite::operator=(const ProbabilisticSite & s)
{
  Site::operator=(s);
  data_ = s.getData();
  return *this;
}

/****************************************************************************************/

// Methods

// aux method for several methods here
void verifyPdf(const std::vector<std::string> & v) throw (Exception)
{
  double sum = 0;

  // take the sum (where toDouble ensures that v[s] is a decimal number)
  for(std::vector<std::string>::const_iterator s = v.begin(); s != v.end(); ++s)
    sum += TextTools::toDouble(*s,'.','E');

  // then we ensure that the sum is 1
  double tol = 0.000001; // but within a tolerance
  double lowerbound = double(1) - tol;
  double upperbound = double(1) + tol;
  if(sum < lowerbound or sum > upperbound)
    throw Exception("Probabilities do not sum up to 1");
}

/****************************************************************************************/

// another aux method for several methods here
std::vector<std::string> getColumns(const Alphabet * alpha)
{

  std::vector<std::string> cols;
  for(std::vector<std::string>::const_iterator s = alpha->getSupportedChars().begin(); s != alpha->getSupportedChars().end(); ++s) {
    // ensure that we take non-gap resolved characters
    if(!alpha->isGap(*s) && !alpha->isUnresolved(*s))
      cols.push_back(*s);
  }

  return cols;
}

/****************************************************************************************/

void ProbabilisticSite::addElement(const std::vector<std::string> & v) throw (Exception)
{

  // first we check if the 'row' is not larger than the number of
  // columns in Data Table
  if(v.size() > data_.getNumberOfColumns())
    throw DimensionException("ProabilisticSite::addElement", v.size(), data_.getNumberOfColumns());

  // next, we run over the vector to ensure that each entry is a decimal number
  try {
    verifyPdf(v);
  }
  catch(Exception e) {
    throw Exception(std::string("ProbabilisticSite::addElement. ") + e.what());
  }

  // now we add this 'row', to the Data Table, padding the end with
  // 0's should its length be smaller than the number of columns in
  // the Data Table
  if(v.size() < data_.getNumberOfColumns()) {
    std::vector<std::string> w(v);
    w.resize(data_.getNumberOfColumns(),"0");
    // Note that addRow can throw a TableRowNamesException, but since
    // the construction of ProbabilisticSite ensures a DataTable with
    // no row names, this cannot happen, so we don't check for this
    data_.addRow(w);
  }
  else {
    data_.addRow(v);
  }
}

/****************************************************************************************/

void ProbabilisticSite::deleteElement(size_t pos) throw (IndexOutOfBoundsException)
{

  /*
    Note: looking at the deleteRow function of Data Table, it reveals
    that DataTables may have heterogeneous column lengths
    ... something I didn't think of before.  This could cause troubles
    down the road because it's not checked against in setData (below).
    For this bppAncestor application, we're reading and writing only
    aligned sequences, so we'll never have a problem here, but if we
    go more general with this Probabilistic-, we'll have to address
    this issue -- add it to the long list of improvements should
    Probabilisitc- become useful to more people
  */

   data_.deleteRow(pos); 
}

/****************************************************************************************/

/*
  the big drawback of not (creating and then) inheriting also from
  some sort of 'ProbabilisticSymbolList' is that we duplicate this
  method from ProbabilisticSequence ... but it's not worth the risk of
  possible conflicts that may arise due to the fact that Site inherits
  from BasicSymbolList.  Again, this will be fixed if we ever come to
  add this properly -- making Site into 'BasicSite' and having a
  'Site' interface from which 'BasicSite' would inherit from (and also
  BasicSymbolList)
*/

void ProbabilisticSite::setData(const DataTable & data) throw (Exception)
{

  // first, if table has column names, we run these names against the
  // alphabet to see if they match.  Note: we ignore row names -- they
  // serve us no purpose here
  if(data.hasColumnNames()) {
    
    // loop through names in DataTable.  Note: getColumnNames could
    // throw a NoTableColumnNamesException, but we don't try to catch
    // this because we did a check above for hasColumnNames
    const std::vector<std::string> & arr = data.getColumnNames();
    for(std::size_t i = 0; i < arr.size(); ++i)
      if(!getAlphabet()->isCharInAlphabet(arr[i]))
	throw BadCharException(arr[i], "ProbabilisticSite::setData", getAlphabet());

    // NOTE : for some reason, I cannot iterate over
    // data.getColumnsNames() ... i.e., the way of looping below
    // caused segmentation faults ... bizarre
    /*
    for(std::vector<std::string>::iterator i = data.getColumnNames().begin(); i != data.getColumnNames().end(); ++i)
      if(!alphabet_->isCharInAlphabet(*i))
	throw BadCharException(*i, "ProbabilisticSequence::setData", alphabet_);
    */
  }
  else { // DataTable has no column names

    // hence, we first check if width of DataTable is not larger than
    // the alphabet size.  Note: getSize returns the number of
    // *resolved* characters (what we want, I think)
    if(data.getNumberOfColumns() != getAlphabet()->getSize())
      throw DimensionException("ProabilisticSite::setData", data.getNumberOfColumns(), getAlphabet()->getSize());
  }

  // in either case, the check passes, so now we do a pass over the
  // table to ensure that each entry is a decimal number, and that
  // each row sums up to 1
  for(std::size_t i = 0; i < data.getNumberOfRows(); ++i) {
    try {
      verifyPdf(data.getRow(i));
    }
    catch(Exception e) {
      throw Exception(std::string("ProbabilisticSite::setData. ") + e.what());
    }
  }

  data_ = data; // final check passes, data_ becomes DataTable

  // now, we work with the columns of our DataTable, in the case that
  // it has no column names
  if(!data.hasColumnNames()) {

    /*
      we associate the columns of DataTable with the (non-gap
      resolved) characters of the alphabet ... this will work with,
      i.e., binary alphabets and DNA alphabets (probably only going to
      use ProbabilisticSequence for binary alphabets anyway)
    */

    std::vector<std::string> cols = getColumns(getAlphabet());

    // set the names.  Note: there should not be a DimensionException
    // because we check above for size, but in any case this method
    // throws DimensionExceptions.  This method does not, however,
    // throw DuplicatedTableColumnNameExceptions, however, we assume
    // that Alphabet already disallows duplicated characters

    data_.setColumnNames(cols);
  }
}
