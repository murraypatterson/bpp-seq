//
// File: ProbabilisticSite.h
// Created by: Murray Patterson
// Created on: Thursday Oct 30
//

#ifndef _PROBABILISTIC_SITE_H_
#define _PROBABILISTIC_SITE_H_

#include "Site.h"
#include <Bpp/Numeric/DataTable.h>

namespace bpp
{

  /*
    The ProbabilisticSite class : this extends the site class to allow
    more complex objects than simply characters in a sequence (and
    hence, site, also).  It's a workaround to the fact that this is
    not already possible, i.e., that all sequences and sites, even the
    very basic interfaces at the root of bpp class heirarchy
    represent, at the base, a sequence as a vector<int>.  Perhaps this
    should remain this way, i.e., for speed? simplicity? (it's hard to
    tell at this point)

    So, for the moment (or in general), I extend the Site class to
    allow these more complex objects.  Because I'm doing this to serve
    my immediate purposes (working with ProbabilisticSequences), this
    complex object is a bpp::DataTable, i.e., a character is now a row
    in this DataTable, which encodes the discrete PDF over the
    alphabet at a site (like in ProbabilisticSeqeunce).  I call this
    "content" 'data_' following the same design as
    ProbabilisticSequence, because 'content_' exists already.

    In addition to this, I don't follow the pattern of other classes,
    in defining a "ProbabilisticSymbolList" which both
    ProbabilisticSequence and ProbabilisticSite could inherit from,
    because Site inherits from BasicSymbolList -- yet Sequence does
    not, which seems inconsitent, since Site is the aligned analog of
    Sequence if I understand properly** -- and I want to avoid a
    posslible multiple-inheritence shit-show of a class that inherits
    from both a "Probabilistic-" and BasicSymbolList, because
    ProbabilisticSite must inherit from Site for everything to run
    smoothly, like in VectorSiteContainer, for example

    ** I suppose in an ideal world, one would have a Site class, which
    is completely analagous to Sequence, and then there's be a
    BasicSite class which inherits from Site and BasicSymbolList, just
    like BasicSequence inherits from Sequence and BasicSymbolList
  */

  class ProbabilisticSite :
    public Site
  {

  private :

    DataTable data_;

  public :

    // Constructors

    ProbabilisticSite(const Alphabet * alpha);

    ProbabilisticSite(const Alphabet * alpha, int position);

    ProbabilisticSite(const DataTable & data, const Alphabet * alpha) throw (Exception);

    ProbabilisticSite(const DataTable & data, const Alphabet * alpha, int position) throw (Exception);

    // Copy Constructors

    ProbabilisticSite(const Site & s);

    ProbabilisticSite(const ProbabilisticSite & s);

    // Assignment Operator

    ProbabilisticSite & operator=(const ProbabilisticSite & s);

    // Clone

    ProbabilisticSite * clone() const { return new ProbabilisticSite(*this); }

    // Destructor

    virtual ~ProbabilisticSite() {}

  public :

    /*
      For now, we just override the following functions because it's
      all we will need for interfacing with VectorSiteContainer for
      our likelihood computation.  We don't add an addElement with
      position, because DataTable doesn't have an addRow with position
      (only with name) function.  Also we don't add a setElement,
      because DataTable doesn't have a setRow function.  Maybe we'll
      add this later (along with more profound changes such as that
      mentioned above**), should many find Probabilistic- to be useful
    */

    virtual size_t size() const { return static_cast<size_t>(data_.getNumberOfRows()); }

    void addElement(const std::vector<std::string> & v) throw (Exception);

    void deleteElement(size_t pos) throw (IndexOutOfBoundsException);

    virtual const int & operator[](size_t i) const { return *(new int(0)); }
    virtual int & operator[](size_t i) { return *(new int(0)); }

    // duplicates from ProbabilisticSequence (see commentary in .cpp file)
    const DataTable & getData() const { return data_; }

    void setData(const DataTable & data) throw (Exception);

  };

} // end of namespace bpp

#endif // _PROBABILISTIC_SITE_H_
